# BCST Themes Gallery

### default
![Default](default.jpg)
*Description*:
- Author: GaugeK
- Url: https://gitlab.com/GaugeK/startpage

### hungry-hobo
![Hungry Hobo](hungry-hobo.jpg)
*Description*:
- Author: hungry-hobo
- Url: https://github.com/Hungry-Hobo/Homepage

### home-term
![Home Term](home-term.jpg)
*Description*:
- Author: Nytly, Teiem
- Url Nytly: https://notabug.org/nytly/home
- Url Teiem: https://github.com/Teiem/homeFork/tree/gh-pages

### qhungg
![Qhungg](qhungg.jpg)
*Description*:
- Author: Qhungg289
- Url: https://github.com/qhungg289/startpage

### hrstv
![Hrstv](hrstv.jpg)
*Description*:
- Author: m_hrstv
- Url: https://www.reddit.com/r/startpages/comments/etzaih/my_first_attempt_at_creating_a_starpage_from/

### afternoon
![Afternoon](afternoon.jpg)
*Description*:
- Author: anton25360
- Url: https://github.com/anton25360/Startpage-live
